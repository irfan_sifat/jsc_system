<?php
if (!isset($_SESSION)) {
    session_start();

    require "backEnds/db.php";
}

if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1) {
    $name = $_SESSION['name'];
    $acc_type = $_SESSION['acc_type'];
} else {
    ?>
    <script>
        alert("You are not logged In. Please Log In first.");
        window.location = "index.php";
    </script>
<?php
}

?>




<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">JSC</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="dashboard.php?profile">Home</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link" href="creatememo.php">Create Memo</a>
            </li> -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Create Memo
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="createprmemo.php">Create JSC Products</a>
                    <hr>
                    <a class="dropdown-item" href="createembmemo.php">Create JSC Embroidery</a>
                </div>
            </li>
            <?php
            if ($acc_type == 'admin') {
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="createexe.php">Create Executive</a>
                </li>
            <?php
            }
            ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Others
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <!-- <a class="dropdown-item" href="dashboard.php?profile">Profile</a> -->
                    <!-- <hr> -->
                    <a class="dropdown-item" href="jscprmemo.php">JSC Products Memo</a>
                    <hr>
                    <a class="dropdown-item" href="jscembmemo.php">JSC EMB Memo</a>
                    <hr>
                    <?php
                    if ($acc_type == 'admin') {
                        ?>
                        <a class="dropdown-item" href="dashboard.php?executive">Executive List</a>
                        <hr>
                    <?php
                    }
                    ?>
                    <a class="dropdown-item" href="#">Reset Password</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?logout=1">Log Out</a>
            </li>
        </ul>
        <span class="navbar-text">
            Log In as <strong class="text-white fs-20 m-l-10"><?= $name ?></strong>
        </span>
    </div>
</nav>