<?php
require "./backEnds/db.php";
require "./backEnds/functions.php";

if (!isset($_SESSION)) session_start();

// $members = $mysqli->query(allSearch("executive"));
$members = $mysqli->query(oneConSearch("executive", "active", "1"));
?>
<div class="m-t-30">
    <div id="executivetablepage">
        <center>
            <h2>Executive List</h2>
            <hr>
        </center>
        <table class="table table-dark fs-11">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Account Type</th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                while ($row = $members->fetch_assoc()) {
                    ?>
                    <tr>
                        <th scope="row"><?= $i ?></th>
                        <td><?= $row['acc_type']; ?></td>
                        <td><?= $row['name']; ?></td>
                        <td><?= $row['phone']; ?></td>
                        <td><?= $row['email']; ?></td>
                        <?php
                            $create_date = date("F j, Y, g:i a", strtotime($row['create_time']));
                            ?>
                        <td><?= $create_date; ?></td>
                        <?php
                            if ($row['acc_type'] == 'user') {
                                ?>
                            <td><a href="backEnds/executiveupdate.php?delete&id=<?= $row['id']; ?>">Delete</a>/<a href="backEnds/executiveupdate.php?admin&id=<?= $row['id']; ?>">Admin</a></td>
                        <?php
                            }else{
                                ?>
                                <td></td>
                                <?php
                            }
                            ?>
                    </tr>
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
</div>