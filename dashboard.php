<?php

require "./backEnds/db.php";

if (!isset($_SESSION)) session_start();


if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1) {
    $email = $_SESSION['email'];
    $phone = $_SESSION['phone'];
    $acc_type = $_SESSION['acc_type'];
    $create_date = $_SESSION['create_date'];

    $create_date = date("F j, Y, g:i a", strtotime($create_date));
} else {
    ?>
    <script>
        alert("You are not logged In. Please Log In first.");
        window.location = "index.php";
    </script>
<?php
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <title>JSC System</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <!-- <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
    <script src="https://use.fontawesome.com/7b8325237f.js"></script>
</head>

<body>

    <?php
    include "./layouts/nav.php";
    ?>

    <div class="dash-page">
        <div class="sidebar p-t-20 fs-20">
            <?php
            if (isset($_GET['profile'])) {
                ?>
                <h1 id="dashSideHead">Profile</h1>
            <?php
            } elseif (isset($_GET['executive'])) {
                ?>
                <h1>Executive&nbsp;List</h1>
            <?php
            }
            ?>
        </div>

        <div class="sidebar-content">
            <div class="container">
                <?php
                if (isset($_GET['profile'])) {
                    include "./layouts/profile.php";
                } elseif (isset($_GET['executive'])) {
                    include "./layouts/executivelist.php";
                }
                ?>
            </div>
        </div>

    </div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>
    <script src="js/custom.js"></script>


</body>

</html>