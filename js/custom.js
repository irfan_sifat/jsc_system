const add = () => {
    var form = document.getElementById('itemBox');

    for (var i = 0; i < 1000; i++) {
        var check = document.getElementById('td' + i);
        if (check) {
            console.log("Existing " + i);
        } else {
            var arr = [i,i+1,i+2,i+3,i+4,i+5,i+6,i+7];
                form.insertAdjacentHTML('beforeend', '<tr id="itemElement" class="br-1"> <td><input class="form-control" type="text" id="td' + arr[0] + '" name="td' + arr[0] + '"></td> <td><input class="form-control" type="text" id="td' + arr[1] + '" name="td' + arr[1] + '"></td> <td><input class="form-control" type="text" id="td' + arr[2] + '" name="td' + arr[2] + '" style="width:55px"></td> <td><input class="form-control" id="td' + arr[3] + '" type="text" name="td' + arr[3] + '" style="width:55px"></td> <td><input class="form-control" type="text" id="td' + arr[4] + '" name="td' + arr[4] + '" style="width:55px"></td> <td><input class="form-control" type="text" id="td' + arr[5] + '" name="td' + arr[5] + '"></td> <td><input class="form-control" type="number" id="td' + arr[6] + '" name="td' + arr[6] + '"></td> <td><input class="form-control" type="text" id="td' + arr[7] + '" name="td' + arr[7] + '"></td> <td><a onclick="deleterow(this)">Delete</a></td> </tr>');
            
            
            break;
        }
    }
}

const pre = () => {
    var client_name = document.getElementById("clientNameP").value;
    document.getElementById("clientNameC").innerHTML = "Client: " + client_name;

    var client_address = document.getElementById("addressP").value;
    document.getElementById("addressC").innerHTML = "Address: " + client_address;

    var client_ref = document.getElementById("refP").value;
    document.getElementById("refC").innerHTML = "Ref. " + client_ref;

    var client_date = document.getElementById("dateP").value;
    document.getElementById("dateC").innerHTML = "Date: " + client_date;

    var client_attn = document.getElementById("attnP").value;
    document.getElementById("attnC").innerHTML = "ATTN. " + client_attn;

    var client_trans = document.getElementById("transP").value;
    document.getElementById("transC").innerHTML = "Transport No. " + client_trans;



    var item = document.querySelectorAll('#itemElement'); // How many Memo Row (<tr>)

    document.getElementById("count").value = item.length;

    var preItem = document.getElementById('itemBoxPreview'); //Preview tbody
    preItem.innerHTML = " "; // Null The Preview Table

    for (var i = 0; i < item.length; i++) { // Insert all Row (<tr>) for Memo without Item
        preItem.insertAdjacentHTML('beforeend', "<tr id=" + i + " class='br-1'>");
    }

    var quantity = 0;

    for (var i = 0; i < item.length; i++) {

        var enterItem = document.getElementById(i); // Find Row (<tr>) for insert Item (<td>)

        var itemList = item[i].querySelectorAll("input"); // Find Item (<td>) for specific Row (<tr>)

        for (var j = 0; j < itemList.length; j++) { //Insert All Item (<td>) For a specific Row (<tr>)
            enterItem.insertAdjacentHTML('beforeend', "<td name=" + i + "store" + j + "'>" + itemList[j].value + "</td>");
        }

        quantity = quantity + parseInt(itemList[6].value);
    }

    document.getElementById("count_quantity").value = quantity;
    preItem.insertAdjacentHTML('beforeend', "<tr class='br-1' style='background-color: lightgray;line-height:10px'> <td colspan='6'>Total</td> <td>" + quantity + "</td> <td></td> </tr>");

}

const deleterow = (o) => {
    var p = o.parentNode.parentNode;
    p.parentNode.removeChild(p);

    let row_count = document.getElementById("count").value;
    row_count = row_count - 1;
    document.getElementById("count").value = row_count;
}



const add_emb = () => {
    var form = document.getElementById('itemBox');

    for (var i = 0; i < 1000; i++) {
        var check = document.getElementById('td' + i);
        if (check) {
            console.log("Existing " + i);
        } else {
            var arr = [i,i+1,i+2,i+3,i+4,i+5,i+6,i+7,i+8,i+9,i+10,i+11];
                form.insertAdjacentHTML('beforeend', '<tr id="itemElement" class="br-1"> <td><input class="form-control" type="text" id="td' + arr[0] + '" name="td' + arr[0] + '"></td> <td><input class="form-control" type="text" id="td' + arr[1] + '" name="td' + arr[1] + '"></td> <td><input class="form-control" type="text" id="td' + arr[2] + '" name="td' + arr[2] + '"></td> <td><input class="form-control" id="td' + arr[3] + '" type="text" name="td' + arr[3] + '"></td> <td><input class="form-control" type="text" id="td' + arr[4] + '" name="td' + arr[4] + '"></td> <td><input class="form-control" type="text" id="td' + arr[5] + '" name="td' + arr[5] + '"></td> <td><input class="form-control" type="text" id="td' + arr[6] + '" name="td' + arr[6] + '"></td> <td><input class="form-control" type="text" id="td' + arr[7] + '" name="td' + arr[7] + '"></td> <td><input class="form-control" type="text" id="td' + arr[8] + '" name="td' + arr[8] + '"></td> <td><input class="form-control" type="text" id="td' + arr[9] + '" name="td' + arr[9] + '"></td> <td><input class="form-control" type="number" id="td' + arr[10] + '" name="td' + arr[10] + '"></td> <td><input class="form-control" type="text" id="td' + arr[11] + '" name="td' + arr[11] + '"></td> <td><a onclick="deleterow(this)">Delete</a></td> </tr>');
            
            
            break;
        }
    }
}


const pre_emb = () => {
    var client_name = document.getElementById("clientNameP").value;
    document.getElementById("clientNameC").innerHTML = "Client: " + client_name;

    var client_address = document.getElementById("addressP").value;
    document.getElementById("addressC").innerHTML = "Address: " + client_address;

    var client_ref = document.getElementById("refP").value;
    document.getElementById("refC").innerHTML = "Ref. " + client_ref;

    var client_date = document.getElementById("dateP").value;
    document.getElementById("dateC").innerHTML = "Date: " + client_date;

    var client_attn = document.getElementById("attnP").value;
    document.getElementById("attnC").innerHTML = "Style/PP No. " + client_attn;

    var client_trans = document.getElementById("transP").value;
    document.getElementById("transC").innerHTML = "Transport No. " + client_trans;

    //sub Heading value set

    for(var k = 0;k < 8; k++){
        var sub_th = document.getElementById("th"+k).value;
        document.getElementById("thp"+k).innerHTML = sub_th;
    }


    var item = document.querySelectorAll('#itemElement'); // How many Memo Row (<tr>)

    document.getElementById("count").value = item.length;

    var preItem = document.getElementById('itemBoxPreview'); //Preview tbody
    preItem.innerHTML = " "; // Null The Preview Table

    for (var i = 0; i < item.length; i++) { // Insert all Row (<tr>) for Memo without Item
        preItem.insertAdjacentHTML('beforeend', "<tr id=" + i + " class='br-1'>");
    }

    var quantity = 0;

    for (var i = 0; i < item.length; i++) {

        var enterItem = document.getElementById(i); // Find Row (<tr>) for insert Item (<td>)

        var itemList = item[i].querySelectorAll("input"); // Find Item (<td>) for specific Row (<tr>)

        for (var j = 0; j < itemList.length; j++) { //Insert All Item (<td>) For a specific Row (<tr>)
            enterItem.insertAdjacentHTML('beforeend', "<td name=" + i + "store" + j + "'>" + itemList[j].value + "</td>");
        }

        quantity = quantity + parseInt(itemList[10].value);
    }

    document.getElementById("count_quantity").value = quantity;
    preItem.insertAdjacentHTML('beforeend', "<tr class='br-1' style='background-color: lightgray;line-height:10px'> <td colspan='10'>Total</td> <td>" + quantity + "</td> <td></td> </tr>");

}