<?php

require "backEnds/db.php";
require "backEnds/functions.php";
if (!isset($_SESSION)) {
    session_start();
}

$client_list_1 = $mysqli->query(oneConSearch("client", "active", "1"));
$client_list_2 = $mysqli->query(oneConSearch("client", "active", "1"));

$jsc_pr_memo_no = $mysqli->query(lastRowSearch("jsc_products_memo", "id"));
$new_memo_no = $jsc_pr_memo_no->fetch_assoc()['sl_no'];
$new_memo_no = $new_memo_no + 1;


?>



<!DOCTYPE html>
<html lang="en">

<head>
    <title>JSC System</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>

<body>

    <?php
    include "./layouts/nav.php";
    ?>


    <center>
        <hr>
        <h3>JSC Products</h3>
        <hr>
    </center>

    <div class="memo-form-page container m-t-50">
        <center>
            SL No. <?= $new_memo_no; ?>
        </center>
        <form action="backEnds/prmemostore.php" method="POST" onchange="pre()">
            <!-- Memo Info  -->
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Client Name</label>
                    <select class="form-control" id="clientNameP" name="clientNameP">
                        <option selected disabled value="">---Choose---</option>
                        <?php
                        while ($row_cn = $client_list_1->fetch_assoc()) {
                            ?>
                            <option value="<?= $row_cn['name'] ?>"><?= $row_cn['name'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <!-- <input type="text" class="form-control" id="clientNameP" name="clientNameP" placeholder="Client Name"> -->
                </div>
                <div class="form-group col-md-4">
                    <label>Address</label>
                    <select class="form-control" id="addressP" name="addressP">
                        <option selected disabled value="">---Choose---</option>
                        <?php
                        while ($row_ca = $client_list_2->fetch_assoc()) {
                            ?>
                            <option value="<?= $row_ca['address'] ?>"><?= $row_ca['address'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <!-- <input type="text" class="form-control" id="addressP" name="addressP" placeholder="Address"> -->
                </div>
                <div class="form-group col-md-4">
                    <label>Ref.</label>
                    <input type="text" class="form-control" id="refP" name="refP" placeholder="Ref.">
                </div>

                <div class="form-group col-md-4">
                    <label>ATTN.</label>
                    <input type="text" class="form-control" id="attnP" name="attnP" placeholder="ATTN.">
                </div>
                <div class="form-group col-md-4">
                    <label>Date</label>
                    <input type="text" class="form-control" id="dateP" name="dateP" placeholder="Date">
                </div>
                <div class="form-group col-md-4">
                    <label>Transport No.</label>
                    <input type="text" class="form-control" id="transP" name="transP" placeholder="Transport No.">

                    <input type="text" class="form-control" id="count" name="count" hidden>
                    <input type="text" class="form-control" id="count_quantity" name="count_quantity" hidden>
                </div>
            </div>

            <!-- Item Info -->
            <div>
                <table class="table">
                    <thead>
                        <tr class="br-1">
                            <th scope="col" rowspan="2">Style Number</th>
                            <th scope="col" rowspan="2">Description</th>
                            <th scope="col" colspan="3">Size</th>
                            <th scope="col" rowspan="2">PLY/THK</th>
                            <th scope="col" rowspan="2">Quantity</th>
                            <th scope="col" rowspan="2">Remarks</th>
                        </tr>
                    </thead>
                    <tbody id="itemBox">
                        <tr class="br-1">
                            <td></td>
                            <td></td>
                            <th>L</th>
                            <th>W</th>
                            <th>H</th>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr id="itemElement" class="br-1">
                            <td><input class="form-control" type="text" id="td0" name="td0" value=""></td>
                            <td><input class="form-control" type="text" id="td1" name="td1" value=""></td>
                            <td><input class="form-control" type="text" id="td2" name="td2" value="" style="width:55px"></td>
                            <td><input class="form-control" type="text" id="td3" name="td3" value="" style="width:55px"></td>
                            <td><input class="form-control" type="text" id="td4" name="td4" value="" style="width:55px"></td>
                            <td><input class="form-control" type="text" id="td5" name="td5" value=""></td>
                            <td><input class="form-control" type="number" id="td6" name="td6" value=""></td>
                            <td><input class="form-control" type="text" id="td7" name="td7" value=""></td>
                            <td><a onclick="deleterow(this)">Delete</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <br>
            <center>
                <button onclick="add()" type="button" class="btn btn-primary">Add Item</button>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" onclick="pre()">Preview</button>
                <button type="submit" class="btn btn-success">Submit</button>
            </center>
        </form>


        <div class="container m-t-50">
            <!-- Memo Modal View -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="width:850px">
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <h6 style="text-decoration: underline;" id="clientNameC" name="clientNameC"></h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 id="slNoC" name="slNoC">SL NO. <?= $new_memo_no; ?></h6>
                                </div>
                                <br>
                                <div class="col-md-8">
                                    <h6 style="text-decoration: underline;" id="addressC" name="addressC"></h6>
                                </div>
                                <br>
                                <div class="col-md-8">
                                    <h6 style="text-decoration: underline;" id="refC" name="refC"></h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 id="dateC" name="dateC"></h6>
                                </div>
                                <br>
                                <div class="col-md-8">
                                    <h6 style="text-decoration: underline;" id="attnC" name="attnC"></h6>
                                </div>
                                <div class="col-md-4">
                                    <h6 id="transC" name="transC"></h6>
                                </div>
                            </div>
                            <br><br>

                            <!-- Preview Table  -->
                            <div>
                                <table class="table fs-12">
                                    <thead>
                                        <tr class="br-1">
                                            <th scope="col" rowspan="2">Style Number</th>
                                            <th scope="col" rowspan="2">Description</th>
                                            <th scope="col" colspan="3">Size</th>
                                            <th scope="col" rowspan="2">PLY/THK</th>
                                            <th scope="col" rowspan="2">Quantity</th>
                                            <th scope="col" rowspan="2">Remarks</th>
                                        </tr>
                                        <tr class="br-1">
                                            <!-- <td class="br-1"></td> -->
                                            <!-- <td class="br-1"></td> -->
                                            <th scope="col">L</th>
                                            <th scope="col">W</th>
                                            <th scope="col">H</th>
                                            <!-- <td class="br-1"></td> -->
                                            <!-- <td class="br-1"></td> -->
                                            <!-- <td class="br-1"></td> -->
                                        </tr>
                                    </thead>
                                    <tbody id="itemBoxPreview">

                                    </tbody>
                                </table>

                                <div class="memoBottomSign fs-15">
                                    <label>Received the item in good condition</label>
                                    <br><br><br>
                                    <center>
                                        <div class="row">
                                            <label class="col-md-4">Receiver's Signature</label>
                                            <label class="col-md-4">Receiver's Signature</label>
                                            <label class="col-md-4">Receiver's Signature</label>
                                        </div>
                                    </center>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>