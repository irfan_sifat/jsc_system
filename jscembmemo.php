<?php

require "./backEnds/db.php";
require "./backEnds/functions.php";

if (!isset($_SESSION)) session_start();


if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1) {
    $email = $_SESSION['email'];
    $phone = $_SESSION['phone'];
    $acc_type = $_SESSION['acc_type'];
    $create_date = $_SESSION['create_date'];

    $create_date = date("F j, Y, g:i a", strtotime($create_date));

    $jsc_pr_memo = $mysqli->query(allSearch("jsc_emb_memo"));
} else {
    ?>
    <script>
        alert("You are not logged In. Please Log In first.");
        window.location = "index.php";
    </script>
<?php
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <title>JSC System</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <!-- <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>

<body>

    <?php
    include "./layouts/nav.php";
    ?>

    <div class="dash-page">
        <div class="sidebar p-t-20 fs-20">
            <h1>JSC&nbsp;Embroidery</h1>
        </div>

        <div class="sidebar-content">
            <div class="container">

                <div class="m-t-30">
                    <div id="executivetablepage">
                        <center>
                            <h2>Memo List</h2>
                            <hr>
                        </center>
                        <table class="table table-dark fs-15">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Sl N0</th>
                                    <th scope="col">File</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                while ($row = $jsc_pr_memo->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <th scope="row"><?= $i ?></th>
                                        <td><?= $row['sl_no']; ?></td>
                                        <td><a href="memo/jsc_emb/<?=$row['file_name'];?>" target="_blank"><?= $row['file_name']; ?></a></td>
                                    </tr>
                                <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>


</body>

</html>