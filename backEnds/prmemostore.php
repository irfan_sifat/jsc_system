<?php

require "./db.php";
require "./functions.php";
if (!isset($_SESSION)) {
    session_start();
}

$jsc_pr_memo_no = $mysqli->query(lastRowSearch("jsc_products_memo", "id"));
$new_memo_no = $jsc_pr_memo_no->fetch_assoc()['sl_no'];
$new_memo_no = $new_memo_no + 1; 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $name = $_POST['clientNameP'];
    $address = $_POST['addressP'];
    $refP = $_POST['refP'];
    $attnP = $_POST['attnP'];
    $dateP = $_POST['dateP'];
    $transP = $_POST['transP'];

    $count_quantity = $_POST['count_quantity'];
    $rows = $_POST['count'];

    $total_items = $rows * 8;

    $td = array();
    for ($i = 0, $k=0; $i < $total_items;) {
        if (isset($_POST["td$k"])) {
            $td[$i] = $_POST["td$k"];
            $i++;
            $k++;
        }else{
            $k++;
        }
    }
}

require_once "../dompdf/autoload.inc.php";

use Dompdf\Dompdf;

$dompdf = new Dompdf();

$html = '<!DOCTYPE html>
<html lang="en">

<head>
    <title>Memo</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <!--===============================================================================================-->
</head>

<body>

<div class="m-t-20">
    <img style="width:100%; height:150px" src="../images/jsc_pr.jpg" alt="JSC Embroidery">
</div>
<hr>

    <div class="container">
        <div class="container">
            <div>
                <div>
                    <div>
                        <div>
                                <div>
                                    <div class="col-md-8" style="margin-bottom:8px">
                                        <h6 style="text-decoration: underline;display:inline-block;width:100%">Client: ' . $name . '</h6>

                                        <h6 style="display:inline-block;">SL NO. '.$new_memo_no.'</h6>
                                    </div>
                                    
                                    <div class="col-md-8" style="margin-bottom:12px">
                                        <h6 style="text-decoration: underline;">Address: ' . $address . '</h6>
                                    </div>
                                    
                                    <div class="col-md-8" style="margin-bottom:8px">
                                        <h6 style="text-decoration: underline;display:inline-block; width:100%">Ref. ' . $refP . '</h6>

                                        <h6 style="display:inline-block;">Date: ' . $dateP . '</h6>
                                    </div>
                                    
                                    <div class="col-md-8" style="margin-bottom:8px">
                                        <h6 style="text-decoration: underline;display:inline-block; width:100%">ATTN. ' . $attnP . '</h6>

                                        <h6 style="display:inline-block;">Transport No. ' . $transP . '</h6>
                                    </div>
                                </div>
                                <br><br>

                                <!-- Preview Table  -->
                                <div>
                                    <table class="table fs-12" style="line-height:2px">
                                        <thead>
                                            <tr class="br-1">
                                                <th scope="col" rowspan="2" style="width:100px">Style' . " " . 'Number</th>
                                                <th scope="col" rowspan="2" style="width:150px">Description</th>
                                                <th scope="col" colspan="3">Size</th>
                                                <th scope="col" rowspan="2">PLY/THK</th>
                                                <th scope="col" rowspan="2">Quantity</th>
                                                <th scope="col" rowspan="2">Remarks</th>
                                            </tr>
                                            <tr class="br-1">
                                                <!-- <td class="br-1"></td> -->
                                                <!-- <td class="br-1"></td> -->
                                                <th scope="col">L</th>
                                                <th scope="col">W</th>
                                                <th scope="col">H</th>
                                                <!-- <td class="br-1"></td> -->
                                                <!-- <td class="br-1"></td> -->
                                                <!-- <td class="br-1"></td> -->
                                            </tr>
                                        </thead>
                                        <tbody>';
                                        $m = 0;
                                        for ($r = 1; $r <= $rows; $r++) {
                                            $html .= '<tr class="br-1">';

                                            for ($j = 0; $j < 8; $j++) {

                                                $html .=   '<td>' . $td[$m] . '</td>';
                                                $m++;
                                            }

                                            $html .=   '</tr>';
                                    }

                        $html .= '
                                <tr class="br-1" style="background-color: lightgray;line-height:10px"> 
                                <td colspan="6">Total</td> 
                                <td>' . $count_quantity . '</td>
                                <td></td> 
                                </tr>
                                </tbody>
                                    </table>
                                    
                                    <div class="memoBottomSign fs-15" >
                                        <label>Received the item in good condition</label>
                                        <br><br><br>
                                            <div>
                                                <label style="display:inline-block" class="col-md-4">Receivers Signature</label>
                                                <label style="display:inline-block" class="col-md-4">Receivers Signature</label>
                                                <label style="display:inline-block" class="col-md-4">Receivers Signature</label>
                                            </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>

</html>';

$dompdf->load_html($html);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();

$output = $dompdf->output();
file_put_contents('../memo/jsc_products/jsc_pr_'.$new_memo_no.'.pdf', $output);

// $dompdf->stream('jsc_pr_'.$new_memo_no.'', array('Attachment' => 0));

$file_name = 'jsc_pr_'.$new_memo_no.'.pdf';
$insert_sql = "INSERT INTO jsc_products_memo (sl_no, file_name) VALUES ('$new_memo_no','$file_name')";

if($mysqli->query($insert_sql) === TRUE){
    ?>
    <script>
        alert("PDF is created.");
        window.location = "../jscprmemo.php";
    </script>
<?php
}else{
    ?>
        <script>
            alert("Sorry, Something is wrong. Try Again!!");
            window.location = "../createprmemo.php";
        </script>
<?php
}

?>