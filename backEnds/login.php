<?php

require "./db.php";
if (!isset($_SESSION)) session_start();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $phone = $_POST['phoneNumber'];
    $phone = "+88" . $phone;

    $result = $mysqli->query("SELECT * FROM executive WHERE phone = '$phone'");

    if ($result->num_rows > 0) {
        $user = $result->fetch_assoc();
        if (password_verify($_POST['password'], $user['password'])) {

            $active = $user['active'];
            if ($active != 1) {
                ?>
                <script>
                    alert("Sorry, You are deactivated. Contact with Admin");
                    window.location = "../index.php";
                </script>
            <?php
                        } else {
                            $_SESSION['name'] = $user['name'];
                            $_SESSION['phone'] = $user['phone'];
                            $_SESSION['email'] = $user['email'];
                            $_SESSION['acc_type'] = $user['acc_type'];
                            $_SESSION['create_date'] = $user['create_time'];
                            $_SESSION['logged_in'] = 1;

                            header("location: ../dashboard.php?profile");
                        }
                    } else {
                        ?>
            <script>
                alert("Password doesn't match.");
                window.location = "../index.php";
            </script>
        <?php
                }
            } else {
                ?>
        <script>
            alert("Executive doesn't exist with this number. Contact with Admin");
            window.location = "../index.php";
        </script>
<?php
    }
}


?>