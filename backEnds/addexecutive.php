<?php

require "./db.php";
if (!isset($_SESSION)) {
    session_start();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $name = $mysqli->escape_string($_POST['exeName']);
    $phone = $mysqli->escape_string($_POST['phoneNumber']);
    $phone = "+88" . $phone;
    $email = $mysqli->escape_string($_POST['exeEmail']);
    $accType = $mysqli->escape_string($_POST['accType']);
    $password = $mysqli->escape_string($_POST['pass']);

    $result = $mysqli->query("SELECT * FROM executive WHERE phone = '$phone'") or die($mysqli->error());

    if ($result->num_rows > 0) {
        ?>
        <script>
            alert("Executive with this phone number already exist. Try with another phone number");
            window.location = "../createexe.php";
        </script>
        <?php
            } else {
                $password_hash = $mysqli->escape_string(password_hash($password, PASSWORD_BCRYPT));

                $sql = "INSERT INTO executive (acc_type, name, phone, email, password) VALUES ('$accType', '$name', '$phone', '$email', '$password_hash')";

                if ($mysqli->query($sql) === TRUE) {
                    ?>
            <script>
                alert("Executive is created.Thank You.");
                window.location = "../dashboard.php?executive";
            </script>
        <?php
                } else {
                    ?>
            <script>
                alert("Something is wrong. Executive is not created. Try Again.");
                window.location = "../createexe.php";
            </script>
    <?php
            }
        }
    } else {
        ?>
    <script>
        alert("Something is wrong. Executive is not created. Try Again.");
        window.location = "../createexe.php";
    </script>
<?php
}


?>