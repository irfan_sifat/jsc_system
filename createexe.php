<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login V3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>

<body>

    <?php
    include "./layouts/nav.php";
    ?>

    <div class="m-t-50">
        <center>
            <h2>Create An Executive</h2>
        </center>
    </div>
    <div class="memo-form-page container m-t-50">

        <form action="backEnds/addexecutive.php" method="POST">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="exeName">Executive Name</label>
                    <input type="text" class="form-control" id="exeName" name="exeName" placeholder="Executive Name" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">Phone</label>
                    <input type="number" class="form-control" id="phone" name="phoneNumber" placeholder="Phone" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="exeEmail">Executive Email</label>
                    <input type="email" class="form-control" id="exeEmail" name="exeEmail" placeholder="Executive Email" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="pass" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="accType">Executive Type</label>
                    <select id="accType" class="form-control" name="accType" required>
                        <option value="" selected disabled> --Choose one-- </option>
                        <option value="admin">Admin</option>
                        <option value="user">User</option>
                    </select>
                </div>
            </div>
            <br>
            <center>
                <button type="submit" class="btn btn-success">Add Executive</button>
            </center>
        </form>

    </div>



    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <!-- <script src="js/main.js"></script>
    <script src="js/custom.js"></script> -->

</body>

</html>